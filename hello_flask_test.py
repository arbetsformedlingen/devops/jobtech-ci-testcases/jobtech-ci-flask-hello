import unittest

import hello_flask

class HelloWorldTests(unittest.TestCase):
    def test_hello(self):
        self.assertEqual(hello_flask.hello_world(), 'Hello, Flask World!')


if __name__ == '__main__':
    unittest.main()
