FROM python:3.13.2-slim-bookworm
WORKDIR /data
RUN apt-get update &&\
    apt-get install make
COPY Makefile hello_flask.py hello_flask_test.py requirements.txt /data/
RUN make venv deps test &&\
    pip install -r requirements.txt
EXPOSE 5000
CMD ["python", "hello_flask.py"]

