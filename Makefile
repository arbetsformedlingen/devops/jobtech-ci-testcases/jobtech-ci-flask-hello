# setup of local dev env
SHELL := /bin/bash
.PHONY: all

all: venv deps test image

venv:
	python -m venv venv
deps:
	source venv/bin/activate &&  python3 -m pip install pytest pytest-cache pytest-subtests pytest-pylint &&  pip install -r requirements.txt
test:
	source venv/bin/activate &&   python3 -m pytest -o markers=task hello_flask_test.py
image:
	podman build . -t jobtech-ci-flask-hello
rmi:
	podman rmi jobtech-ci-flask-hello
